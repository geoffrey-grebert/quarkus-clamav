/*
* Licensed to the Apache Software Foundation (ASF) under one or more
* contributor license agreements.  See the NOTICE file distributed with
* this work for additional information regarding copyright ownership.
* The ASF licenses this file to You under the Apache License, Version 2.0
* (the "License"); you may not use this file except in compliance with
* the License.  You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package ggrebert.clamav.extension.it;

import java.io.File;
import java.nio.charset.StandardCharsets;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.jboss.resteasy.reactive.PartType;
import org.jboss.resteasy.reactive.RestForm;
import org.jboss.resteasy.reactive.server.ServerExceptionMapper;

import javax.inject.Inject;
import ggrebert.clamav.ClamAVAsyncClient;
import ggrebert.clamav.ClamAVClient;
import ggrebert.clamav.extension.runtime.ClamAVException;
import io.smallrye.mutiny.Uni;

@Path("/clamav-extension")
@Produces(MediaType.TEXT_PLAIN)
@ApplicationScoped
public class ClamavExtensionResource {
    // add some rest methods here

    @Inject ClamAVAsyncClient asyncClamAV;
    @Inject ClamAVClient clamAV;

    @GET
    public String hello() {
        return "Hello clamav-extension";
    }

    @GET
    @Path("/stats")
    public Uni<String> stats() {
        return asyncClamAV.stats();
    }

    @GET
    @Path("/blocking/stats")
    public String statsBlocking() {
        return clamAV.stats();
    }

    @GET
    @Path("/healthy")
    public Uni<String> healthy() {
        return asyncClamAV.healthy()
            .map(b -> Boolean.TRUE.equals(b) ? "healthy" : "unhealthy");
    }

    @GET
    @Path("/version")
    public Uni<String> version() {
        return asyncClamAV.version();
    }

    @GET
    @Path("/reload")
    public Uni<String> reload() {
        return asyncClamAV.reload();
    }

    @GET
    @Path("/eicar")
    public Uni<String> scan() {
        byte[] EICAR = "X5O!P%@AP[4\\PZX54(P^)7CC)7}$EICAR-STANDARD-ANTIVIRUS-TEST-FILE!$H+H*".getBytes(StandardCharsets.US_ASCII);
        return asyncClamAV.scan(EICAR);
    }

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @RequestBody(content = @Content(schema = @Schema(implementation = FormData.class)))
    @Path("/scan")
    public Uni<String> scan(FormData form) {
        return asyncClamAV.scan(form.file);
    }

    public static class FormData {

        @RestForm("file")
        @PartType(MediaType.APPLICATION_OCTET_STREAM)
        @Schema(format = "binary", type = SchemaType.STRING, implementation = String.class, required = true)
        File file;
    }

    @ServerExceptionMapper
    public Uni<Response> handleException(ClamAVException e) {
        return Uni.createFrom()
            .item(Response.status(500).entity(e.getLocalizedMessage()).build());
  }
}

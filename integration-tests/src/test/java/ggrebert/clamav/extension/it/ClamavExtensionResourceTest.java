package ggrebert.clamav.extension.it;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;

import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class ClamavExtensionResourceTest {

    @Test
    public void testHelloEndpoint() {
        given()
                .when().get("/clamav-extension")
                .then()
                .statusCode(200)
                .body(is("Hello clamav-extension"));
    }
}

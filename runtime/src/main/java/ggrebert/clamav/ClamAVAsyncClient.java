package ggrebert.clamav;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ConnectException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.time.Duration;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Pattern;

import javax.inject.Inject;

import org.jboss.logging.Logger;

import ggrebert.clamav.extension.runtime.ClamAVConfig;
import ggrebert.clamav.extension.runtime.ClamAVException;
import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import io.vertx.core.net.NetClientOptions;
import io.vertx.mutiny.core.Vertx;
import io.vertx.mutiny.core.buffer.Buffer;
import io.vertx.mutiny.core.net.NetClient;
import io.vertx.mutiny.core.net.NetSocket;

public class ClamAVAsyncClient {

    private static final Logger LOG = Logger.getLogger(ClamAVAsyncClient.class);
    public static final int CHUNK_SIZE = 2048;
    private static final byte[] INSTREAM = "zINSTREAM\0".getBytes();
    private static final byte[] PING = "zPING\0".getBytes();
    private static final byte[] STATS = "nSTATS\n".getBytes();
    private static final byte[] RELOAD = "nRELOAD\n".getBytes();
    private static final byte[] VERSION = "zVERSION\0".getBytes();

    @Inject ClamAVConfig config;

    @Inject Vertx vertx;

    private static final Pattern HOST_PATTERN = Pattern.compile("^(?<host>[^:]+):(?<port>\\d+)$");

    public Uni<Boolean> healthy() {
        return cmd(PING)
            .map(s -> s.startsWith("PONG"))
            .onFailure().recoverWithItem(Boolean.FALSE);
    }

    public Uni<String> version() {
        return cmd(VERSION).map(s -> s.substring(0, s.length() - 1));
    }

    public Uni<String> stats() {
        return cmd(STATS);
    }

    public Uni<String> reload() {
        return cmd(RELOAD);
    }

    public Uni<String> scan(InputStream stream) {
        AtomicReference<Context> context = new AtomicReference<>();
        ClamAVServer server = getServer();
        CompletableFuture<String> message = new CompletableFuture<>();
        Buffer buffer = Buffer.buffer();

        return vertx.createNetClient(getNetClientOptions())
            .connect(server.port, server.host)
            // .map(s -> s.handler(b -> this.handle(b, message, s)))
            .map(s -> s.handler(b -> {
                LOG.info("new message");
                buffer.appendBuffer(b);
            }))
            .map(s -> s.endHandler(() -> {
                LOG.info("end handler");
                message.complete(buffer.toString(StandardCharsets.US_ASCII));
            }))
            .map(s -> s.closeHandler(() -> {
                LOG.info("close handler");
            }))
            .map(s -> s.drainHandler(() -> {
                LOG.info("drain handler");
            }))
            .map(s -> s.exceptionHandler(t -> {
                LOG.error("exception handler", t);
            }))
            .map(s -> s.setWriteQueueMaxSize(0))
            .chain(s -> s.write(Buffer.buffer(INSTREAM)).replaceWith(s))
            .invoke(s -> context.set(new Context(s)))
            .onItem()
            .transformToMulti(s -> {
                LOG.info("convert to multi");
                return Multi.createFrom().generator(context::get, (n, emitter) -> {
                    try {
                        LOG.info("read");
                        n.read = stream.read(n.buffer);
                        LOG.info("buffer size: " + n.buffer.length);
                        if (n.read == -1) {
                            LOG.info("complete");
                            emitter.complete();
                        }
                        else {
                            LOG.info("emit");
                            emitter.emit(n);
                        }
                    } catch (IOException e) {
                        LOG.error("fail", e);
                        emitter.fail(e);
                    }

                    return n;
                });
            })
            .invoke(c -> LOG.info("send context"))
            .onItem()
            .castTo(Context.class)
            .onItem()
            .transformToUniAndConcatenate(c ->
                c.socket
                    .write(Buffer.buffer(c.read))
                    .invoke(b -> LOG.info("write chunk size"))
                    .invoke(v -> c.socket.write(Buffer.buffer(c.buffer)))
                    .invoke(b -> LOG.info("write buffer"))
                    .replaceWith(c)
                    .onFailure()
                    .invoke(e -> LOG.error("error on Uni: " + e.getLocalizedMessage(), e))
            )
            .invoke(c -> LOG.info("Before collect"))
            .collect()
            .last()
            .invoke(c -> LOG.info("last"))
            .chain(c -> c.socket.write(Buffer.buffer(ByteBuffer.allocate(4).putInt(0).array())))
            // .chain(c -> c.socket.end())
            .chain(() -> Uni.createFrom().completionStage(message))
            ;
    }

    public Uni<String> scan(byte[] bytes) {
        return scan(new ByteArrayInputStream(bytes));
    }

    public Uni<String> scan(File file) {
        try {
            LOG.info("size: " + Files.readAllBytes(file.toPath()).length );
            return scan(new FileInputStream(file));
        } catch (IOException e) {
            return Uni.createFrom().failure(e);
        }
    }

    private Uni<NetSocket> connect() {
        return Uni.createFrom().item(vertx.createNetClient(getNetClientOptions()))
            .chain(c -> getServer().connect(c))
            .onFailure(ConnectException.class)
            .invoke(t -> LOG.debugv(t.getLocalizedMessage()))
            .onFailure(ConnectException.class)
            .transform(ClamAVException.ClamAVConnectionException::new)
            .onFailure(ClamAVException.ClamAVConnectionException.class)
            .retry()
                .withBackOff(Duration.ofMillis(config.retryBackOff))
                .withJitter(config.retryJitter)
                .atMost(config.retryMaxAttempts)
            ;
    }

    private Uni<String> cmd(byte[] cmd) {
        CompletableFuture<String> message = new CompletableFuture<>();
        Buffer buffer = Buffer.buffer();

        return connect()
            // .map(s -> s.handler(b -> this.handle(b, message, s)))
            .map(s -> s.handler(b -> {
                LOG.info("new message");
                buffer.appendBuffer(b);
            }))
            .map(s -> s.endHandler(() -> {
                LOG.info("end handler");
                message.complete(buffer.toString(StandardCharsets.US_ASCII));
            }))
            .map(s -> s.closeHandler(() -> {
                LOG.info("close handler");
            }))
            .map(s -> s.drainHandler(() -> {
                LOG.info("drain handler");
            }))
            .map(s -> s.exceptionHandler(t -> {
                LOG.error("exception handler", t);
            }))
            .chain(s -> s.write(Buffer.buffer(cmd)))
            // .chain(s -> s.end())
            .chain(() -> Uni.createFrom().completionStage(message));
    }

    private void handle(Buffer buffer, Buffer body) {
        body.appendBuffer(buffer);
    }

    private void handle(Buffer buffer, CompletableFuture<String> message, NetSocket socket) {
        var str = buffer.toString(StandardCharsets.US_ASCII);
        LOG.info("new message: " + str);
        message.complete(str);
        socket.closeAndForget();
    }

    private NetClientOptions getNetClientOptions() {
        return new NetClientOptions()
            // .setConnectTimeout(600000)
            // .setIdleTimeout(3600)
            // .setReadIdleTimeout(3600)
            // .setWriteIdleTimeout(3600)
            // .setSendBufferSize(CHUNK_SIZE)
            // .setSendBufferSize(CHUNK_SIZE)
            // .setConnectTimeout((int) config.heartbeatFrequency.get().toMillis())
            ;
    }

    private ClamAVServer getServer() {
        int randomElementIndex = ThreadLocalRandom.current().nextInt(config.hosts.size());
        return new ClamAVServer(config.hosts.get(randomElementIndex));
    }

    private static class ClamAVServer {
        private final String host;
        private final int port;

        public ClamAVServer(String host) {
            var res = HOST_PATTERN.matcher(host);

            if (!res.matches()) {
                throw new IllegalArgumentException("Invalid host: " + host);
            }

            this.host = res.group("host");
            this.port = Integer.parseInt(res.group("port"));
        }

        private Uni<NetSocket> connect(NetClient client) {
            return client.connect(port, host);
        }
    }

    private static class Context {
        private final NetSocket socket;
        private final byte[] buffer = new byte[CHUNK_SIZE];
        private int read = 0;

        public Context(NetSocket socket) {
            this.socket = socket;
        }
    }

}

package ggrebert.clamav.extension.runtime;

public class ClamAVException extends RuntimeException {

    public ClamAVException() {
        super();
    }

    public ClamAVException(String message) {
        super(message);
    }

    public ClamAVException(String message, Throwable cause) {
        super(message, cause);
    }

    public ClamAVException(Throwable cause) {
        super(cause);
    }

    public static class ClamAVConnectionException extends ClamAVException {
        public ClamAVConnectionException(String message) {
            super(message);
        }

        public ClamAVConnectionException(String message, Throwable cause) {
            super(message, cause);
        }

        public ClamAVConnectionException(Throwable cause) {
            super(cause);
        }
    }

}

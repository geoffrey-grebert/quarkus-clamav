package ggrebert.clamav.extension.runtime;

import java.time.Duration;
import java.util.List;
import java.util.Optional;

import io.quarkus.runtime.annotations.ConfigItem;
import io.quarkus.runtime.annotations.ConfigPhase;
import io.quarkus.runtime.annotations.ConfigRoot;

@ConfigRoot(name = "clamav", phase = ConfigPhase.RUN_TIME)
public class ClamAVConfig {

    public static final String CONFIG_NAME = "clamav";

    /**
     * Configures the ClamAV server addressed (one if single mode).
     * The addresses are passed as {@code host:port}.
     */
    @ConfigItem(defaultValue = "127.0.0.1:3310")
    public List<String> hosts;

    /**
     * The frequency that the driver will attempt to determine the current state of each server in the cluster.
     */
    @ConfigItem
    public Optional<Duration> heartbeatFrequency;

    /**
     * Configures a back-off delay in milliseconds between to attempt to re-subscribe.
     * A random factor (jitter) is applied to increase the delay when several failures happen.
     * 
     * By default, it's set to 300ms.
     */
    @ConfigItem(defaultValue = "300")
    public Integer retryBackOff;

    /**
     * Configures the random factor when using back-off.
     * 
     * By default, it's set to 0.5.
     */
    @ConfigItem(defaultValue = "0.5")
    public Double retryJitter;

    /**
     * Configures the maximum number of attempts to re-subscribe.
     * 
     * By default, it's set to 3.
     */
    @ConfigItem(defaultValue = "3")
    public Integer retryMaxAttempts;
}

package ggrebert.clamav;

import javax.inject.Inject;

public class ClamAVClient {
    
    @Inject ClamAVAsyncClient client;

    public boolean healthy() {
        return Boolean.TRUE.equals(client.healthy().await().indefinitely());
    }

    public String version() {
        return client.version().await().indefinitely();
    }

    public String stats() {
        return client.stats().await().indefinitely();
    }
}

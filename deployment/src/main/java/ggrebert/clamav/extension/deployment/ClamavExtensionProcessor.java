package ggrebert.clamav.extension.deployment;

import ggrebert.clamav.ClamAVAsyncClient;
import ggrebert.clamav.ClamAVClient;
import io.quarkus.arc.deployment.AdditionalBeanBuildItem;
import io.quarkus.deployment.annotations.BuildProducer;
import io.quarkus.deployment.annotations.BuildStep;
import io.quarkus.deployment.builditem.FeatureBuildItem;

class ClamavExtensionProcessor {

    public static final String FEATURE = "clamav-extension";

    @BuildStep
    FeatureBuildItem feature() {
        return new FeatureBuildItem(FEATURE);
    }

    @BuildStep
    void registerBeans(BuildProducer<AdditionalBeanBuildItem> producer) {
        producer.produce(
            new AdditionalBeanBuildItem(ClamAVAsyncClient.class)
        );
        producer.produce(
            new AdditionalBeanBuildItem(ClamAVClient.class)
        );
            // new BeanDefiningAnnotationBuildItem(
            //     DotName.createSimple(ClamAV.class),
            //     DotName.createSimple(ApplicationScoped.class),
            //     false));
    }
}

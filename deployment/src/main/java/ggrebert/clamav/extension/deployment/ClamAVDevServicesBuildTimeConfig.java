package ggrebert.clamav.extension.deployment;

import java.util.Optional;
import java.util.OptionalInt;

import io.quarkus.runtime.annotations.ConfigGroup;
import io.quarkus.runtime.annotations.ConfigItem;

@ConfigGroup
public class ClamAVDevServicesBuildTimeConfig {

    /**
     * If Dev Services for ClamAV has been explicitly enabled or disabled.
     * Dev Services are generally enabled by default,
     * unless there is an existing configuration present.
     */
    @ConfigItem
    public Optional<Boolean> enabled = Optional.empty();

    /**
     * Optional fixed port the dev service will listen to.
     * <p>
     * If not defined, the port will be chosen randomly.
     */
    @ConfigItem
    public OptionalInt port;

    /**
     * The image to use.
     */
    @ConfigItem(defaultValue = "clamav/clamav:latest")
    public String imageName;

    /**
     * Indicates if the ClamAV service managed by Quarkus Dev Services is shared.
     * When shared, Quarkus looks for running containers using label-based service discovery.
     * If a matching container is found, it is used, and so a second one is not started.
     * Otherwise, Dev Services for ClamAV starts a new container.
     * <p>
     * The discovery uses the {@code quarkus-dev-service-clamav} label.
     * The value is configured using the {@code service-name} property.
     * <p>
     * Container sharing is only used in dev mode.
     */
    @ConfigItem(defaultValue = "true")
    public boolean shared;

    /**
     * The value of the {@code quarkus-dev-service-clamav} label attached to the started container.
     * This property is used when {@code shared} is set to {@code true}.
     * In this case, before starting a container, Dev Services for ClamAV looks for a container with the
     * {@code quarkus-dev-service-clamav} label
     * set to the configured value. If found, it will use this container instead of starting a new one. Otherwise, it
     * starts a new container with the {@code quarkus-dev-service-clamav} label set to the specified value.
     * <p>
     * This property is used when you need multiple shared ClamAV service.
     */
    @ConfigItem(defaultValue = "clamav")
    public String serviceName;
}

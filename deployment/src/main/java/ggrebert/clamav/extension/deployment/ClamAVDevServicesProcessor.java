package ggrebert.clamav.extension.deployment;

import java.io.Closeable;
import java.time.Duration;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;

import org.jboss.logging.Logger;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.Network;
import org.testcontainers.utility.DockerImageName;

import io.quarkus.bootstrap.classloading.QuarkusClassLoader;
import io.quarkus.deployment.IsNormal;
import io.quarkus.deployment.annotations.BuildStep;
import io.quarkus.deployment.annotations.BuildSteps;
import io.quarkus.deployment.builditem.DevServicesResultBuildItem;
import io.quarkus.deployment.builditem.DockerStatusBuildItem;
import io.quarkus.deployment.builditem.LaunchModeBuildItem;
import io.quarkus.deployment.builditem.DevServicesResultBuildItem.RunningDevService;
import io.quarkus.deployment.console.ConsoleInstalledBuildItem;
import io.quarkus.deployment.console.StartupLogCompressor;
import io.quarkus.deployment.dev.devservices.GlobalDevServicesConfig;
import io.quarkus.deployment.logging.LoggingSetupBuildItem;
import io.quarkus.devservices.common.ContainerLocator;
import io.quarkus.runtime.LaunchMode;
import io.quarkus.runtime.configuration.ConfigUtils;

@BuildSteps(onlyIfNot = IsNormal.class, onlyIf = GlobalDevServicesConfig.Enabled.class)
public class ClamAVDevServicesProcessor {
    
    private static final Logger log = Logger.getLogger(ClamAVDevServicesProcessor.class);

    /**
     * Label to add to shared Dev Service for ClamAV running in containers.
     * This allows other applications to discover the running service and use it instead of starting a new instance.
     */
    private static final String DEV_SERVICE_LABEL = "quarkus-dev-service-clamav";

    private static final int DEV_SERVICE_PORT = 3310;

    private static final ContainerLocator containerLocator = new ContainerLocator(DEV_SERVICE_LABEL, DEV_SERVICE_PORT);
    private static final String CLAMAV_HOSTS_PROP = "quarkus.clamav.hosts";

    static volatile RunningDevService devService;
    static volatile ClamAVDevServiceCfg cfg;
    static volatile boolean first = true;

    @BuildStep
    public DevServicesResultBuildItem startClamAVDevService(
        DockerStatusBuildItem dockerStatusBuildItem,
        LaunchModeBuildItem launchMode,
        ClamAVBuildTimeConfig clamavBuildTimeConfig,
        Optional<ConsoleInstalledBuildItem> consoleInstalledBuildItem,
        LoggingSetupBuildItem loggingSetupBuildItem,
        GlobalDevServicesConfig devServicesConfig
    ) {
        ClamAVDevServiceCfg configuration = new ClamAVDevServiceCfg(clamavBuildTimeConfig.devservices);

        if (devService != null) {
            boolean shouldShutdownTheService = !configuration.equals(cfg);
            if (!shouldShutdownTheService) {
                return devService.toBuildItem();
            }
            shutdownService();
            cfg = null;
        }

        StartupLogCompressor compressor = new StartupLogCompressor(
                (launchMode.isTest() ? "(test) " : "") + "ClamAV Dev Services Starting:", consoleInstalledBuildItem,
                loggingSetupBuildItem);

        try {
            RunningDevService newDevService = startService(dockerStatusBuildItem, configuration, launchMode,
                    devServicesConfig.timeout);

            if (newDevService != null) {
                devService = newDevService;
            }

            if (devService == null) {
                compressor.closeAndDumpCaptured();
            } else {
                compressor.close();
            }
        } catch (Throwable t) {
            compressor.closeAndDumpCaptured();
            throw new RuntimeException(t);
        }

        if (devService == null) {
            return null;
        }

        // Configure the watch dog
        if (first) {
            first = false;
            Runnable closeTask = () -> {
                if (devService != null) {
                    shutdownService();

                    log.info("Dev Services for ClamAV shut down.");
                }
                first = true;
                devService = null;
                cfg = null;
            };
            QuarkusClassLoader cl = (QuarkusClassLoader) Thread.currentThread().getContextClassLoader();
            ((QuarkusClassLoader) cl.parent()).addCloseTask(closeTask);
        }

        cfg = configuration;
        return devService.toBuildItem();
    }

    private RunningDevService startService(DockerStatusBuildItem dockerStatusBuildItem,
            ClamAVDevServiceCfg config, LaunchModeBuildItem launchMode,
            Optional<Duration> timeout) {
        if (!config.devServicesEnabled) {
            // explicitly disabled
            log.debug("Not starting Dev Services for ClamAV, as it has been disabled in the config.");
            return null;
        }

        // Check if clamav-port or clamav-host are set
        if (ConfigUtils.isPropertyPresent(CLAMAV_HOSTS_PROP)) {
            log.debug("Not starting Dev Services for ClamAV, the " + CLAMAV_HOSTS_PROP + " property is configured.");
            return null;
        }

        if (!dockerStatusBuildItem.isDockerAvailable()) {
            log.warn("Docker isn't working, please configure the ClamAV service location.");
            return null;
        }

        ClamAvContainer container = new ClamAvContainer(DockerImageName.parse(config.imageName), config.fixedExposedPort,
                launchMode.getLaunchMode() == LaunchMode.DEVELOPMENT ? config.serviceName : null);

        final Supplier<RunningDevService> defaultClamavSupplier = () -> {
            // Starting the service
            timeout.ifPresent(container::withStartupTimeout);
            container.start();
            return getRunningDevService(container.getContainerId(), container::close, container.getHost(),
                    container.getPort());
        };

        return containerLocator.locateContainer(config.serviceName, config.shared, launchMode.getLaunchMode())
                .map(containerAddress -> getRunningDevService(containerAddress.getId(), null, containerAddress.getHost(),
                containerAddress.getPort()))
                .orElseGet(defaultClamavSupplier);
    }

    private void shutdownService() {
        if (devService != null) {
            try {
                devService.close();
            } catch (Throwable e) {
                log.error("Failed to stop the ClamAV service", e);
            } finally {
                devService = null;
            }
        }
    }

    private RunningDevService getRunningDevService(String containerId, Closeable closeable, String host, int port) {
        Map<String, String> configMap = Map.of("quarkus.clamav.hosts", host + ":" + port);
        return new RunningDevService(ClamavExtensionProcessor.FEATURE, containerId, closeable, configMap);
    }

    private static final class ClamAVDevServiceCfg {
        private final boolean devServicesEnabled;
        private final String imageName;
        private final Integer fixedExposedPort;
        private final boolean shared;
        private final String serviceName;

        public ClamAVDevServiceCfg(ClamAVDevServicesBuildTimeConfig devServicesConfig) {
            this.devServicesEnabled = devServicesConfig.enabled.orElse(true);
            this.imageName = devServicesConfig.imageName;
            this.fixedExposedPort = devServicesConfig.port.orElse(0);
            this.shared = devServicesConfig.shared;
            this.serviceName = devServicesConfig.serviceName;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }

            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }

            ClamAVDevServiceCfg that = (ClamAVDevServiceCfg) obj;
            return devServicesEnabled == that.devServicesEnabled && Objects.equals(imageName, that.imageName)
                    && Objects.equals(fixedExposedPort, that.fixedExposedPort);
        }

        @Override
        public int hashCode() {
            return Objects.hash(devServicesEnabled, imageName, fixedExposedPort);
        }
    }

    private static final class ClamAvContainer extends GenericContainer<ClamAvContainer> {

        private final int port;
        
        private ClamAvContainer(DockerImageName dockerImageName, int fixedExposedPort, String serviceName) {
            super(dockerImageName);

            port = fixedExposedPort;

            withNetwork(Network.SHARED);
            withExposedPorts(DEV_SERVICE_PORT);

            if (serviceName != null) { // Only adds the label in dev mode.
                withLabel(DEV_SERVICE_LABEL, serviceName);
            }
        }

        @Override
        protected void configure() {
            super.configure();

            if (port > 0) {
                addFixedExposedPort(port, DEV_SERVICE_PORT);
            }
        }

        public int getPort() {
            return getMappedPort(DEV_SERVICE_PORT);
        }
    }
}

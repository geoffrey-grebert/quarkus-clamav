package ggrebert.clamav.extension.deployment;

import io.quarkus.runtime.annotations.ConfigItem;
import io.quarkus.runtime.annotations.ConfigPhase;
import io.quarkus.runtime.annotations.ConfigRoot;

@ConfigRoot(name = "clamav", phase = ConfigPhase.BUILD_TIME)
public class ClamAVBuildTimeConfig {

    /**
     * Configuration for DevServices.
     * DevServices allows Quarkus to automatically start a ClamAV service in dev and test mode.
     */
    @ConfigItem
    public ClamAVDevServicesBuildTimeConfig devservices;

}
